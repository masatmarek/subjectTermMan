//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent } from "uu5g04-hooks";
import "uu_plus4u5g01-bricks";

import Config from "./config/config.js";

import StudentList from "../core/student-list";

import Lsi from "../config/lsi.js";
//@@viewOff:imports

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "Student",
  //@@viewOff:statics
};

export const Student = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    const attrs = UU5.Common.VisualComponent.getAttrs(props);
    return (
      <div {...attrs}>
        <UU5.Bricks.Header
          level="0"
          content={
            <UU5.Bricks.Div>
             Seznam studentů
            </UU5.Bricks.Div>
          }
        />
        {props.params && props.params.id ? <SubjectDetail id={props.params.id} /> : <StudentList />}
      </div>
    );
    //@@viewOff:render
  },
});

export default Student;
