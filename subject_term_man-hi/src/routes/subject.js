//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent } from "uu5g04-hooks";
import "uu_plus4u5g01-bricks";

import Config from "./config/config.js";

import SubjectList from "../core/subject-list";
import SubjectDetail from "../core/subject-detail";

import Lsi from "../config/lsi.js";
//@@viewOff:imports

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "Subject",
  //@@viewOff:statics
};

export const Subject = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:private
    //@@viewOff:private

    //@@viewOn:interface
    //@@viewOff:interface

    //@@viewOn:render
    const attrs = UU5.Common.VisualComponent.getAttrs(props);
    return (
      <div {...attrs}>
        <UU5.Bricks.Header
          level="0"
          content={
            <UU5.Bricks.Div>
              {props.params && props.params.id ? "Přdemět" : <UU5.Bricks.Lsi lsi={Lsi.subjectList.header} />}
            </UU5.Bricks.Div>
          }
        />
        {props.params && props.params.id ? <SubjectDetail id={props.params.id} /> : <SubjectList />}
      </div>
    );
    //@@viewOff:render
  },
});

export default Subject;
