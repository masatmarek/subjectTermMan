//@@viewOn:imports
import * as UU5 from "uu5g04";
import "uu5g04-bricks";
import "uu5tilesg01";
import "uu_plus4u5g01-bricks";
import "uu_plus4u5g01-console";
import UuP from "uu_pg01";
import "uu_pg01-bricks";
import "uu_pg01-tiles";

import Config from "./config/config.js";
import Calls from "../calls";

import ModalHelper from "../helpers/modal-helper.js";
import UpdateSubjectModal from "./update-subject-modal";
//import DeleteSubjectModal from "./delete-subject-modal";

import Lsi from "./subject-tile-lsi.js";
//@@viewOff:imports

export const SubjectTile = UU5.Common.VisualComponent.create({
  //@@viewOn:mixins
  mixins: [UU5.Common.BaseMixin],
  //@@viewOff:mixins

  //@@viewOn:statics
  statics: {
    tagName: Config.TAG + "StudentTile",
    classNames: {
      main: () => Config.Css.css``,
      href: () => Config.Css.css`
        cursor: pointer;
      `,
    },
  },
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    data: UU5.PropTypes.object.isRequired,
    onDelete: UU5.PropTypes.func.isRequired,
    onRelocate: UU5.PropTypes.func.isRequired,
    onUpdate: UU5.PropTypes.func.isRequired,
  },
  //@@viewOff:propTypes

  //@@viewOn:getDefaultProps
  //@@viewOff:getDefaultProps

  //@@viewOn:reactLifeCycle
  getInitialState() {
    this._listDataManager = UU5.Common.Reference.create();
    this._createModal = UU5.Common.Reference.create();
    this._updateModal = UU5.Common.Reference.create();
    return {};
  },
  //@@viewOff:reactLifeCycle

  //@@viewOn:interface
  //@@viewOff:interface

  //@@viewOn:overriding
  //@@viewOff:overriding

  //@@viewOn:private
  _handleUpdate(data) {
    return new Promise((resolve, reject) => {
      Calls.subjectUpdate({
        data,
        done: (dtoOut) => {
          ModalHelper.close();
          resolve(dtoOut);
          this._listDataManager.current.load();
        },
        fail: (failDtoOut) => {
          UU5.Environment.getPage().getAlertBus().setAlert({ colorSchema: "danger", content: failDtoOut.message });
          ModalHelper.close();
          reject(failDtoOut);
        },
      });
    });
  },

  _getTileActionList(tileData) {
    let actions = [
      {
        content: <UU5.Bricks.Lsi lsi={Lsi.updateButton} />,
        onClick: () => {
          this._openUpdateModal(tileData);
        },
        bgStyle: "filled",
        priority: 0,
      },
      {
        content: <UU5.Bricks.Lsi lsi={Lsi.deleteButton} />,
        onClick: () => {
          console.log("delete");
          //this._openDeleteModal(tileData);
        },
        bgStyle: "filled",
        priority: 0,
      },
    ];
    return actions;
  },
  //   _openDeleteModal(data) {
  //     ModalHelper.open(
  //       <UU5.Bricks.Lsi lsi={Lsi.deleteSubject} />,
  //       <DeleteSubjectModal onDelete={this.props.onDelete} code={data.code} name={data.name} />
  //     );
  //   },
  _openUpdateModal(data) {
    this._updateModal.current.open(data);
  },

  _getSubjectInfoLine(name, value) {
    let classNames = this.getClassName();
    return (
      <UU5.Bricks.Div key={name}>
        <strong>
          <UU5.Bricks.Lsi lsi={Lsi[name]} />
        </strong>
        :&nbsp;&nbsp;
        <UU5.Bricks.Span content={value} />
      </UU5.Bricks.Div>
    );
  },
  _setRoute(id) {
    UU5.Environment.setRoute("subject", { id });
  },
  //@@viewOff:private

  //@@viewOn:render
  render() {
    let { name, description, credits, language, id } = this.props.data;
    let classNames = this.getClassName();
    
    return (
      <>
        <UuP.Tiles.ActionTile
          {...this.getMainPropsToPass()}
          key={id}
          actionList={this._getTileActionList(this.props.data)}
          mainAttrs={{
            onClick: () => this._setRoute(id),
          }}
          header={<UU5.Bricks.Lsi lsi={name} />}
          level={4}
          className={classNames.href()}
          maxHeight={200}
          content={
            <>
              {<UU5.Bricks.Lsi lsi={description} />}
              {this._getSubjectInfoLine("credits", credits)}
              {this._getSubjectInfoLine("language", language)}
            </>
          }
        />
        <UpdateSubjectModal ref_={this._updateModal} onUpdate={this.props.onUpdate} />
      </>
    );
  },
  //@@viewOff:render
});

export default SubjectTile;
