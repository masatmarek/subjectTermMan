let Lsi = {
  create: { cs: "Vytvořit", en: "Create" },
  cancel: { cs: "Zrušit", en: "Cancel" },
  required: {
    cs: "Toto pole je povinné.",
    en: "This input is required.",
  },
  publisher: { cs: "Vydavatel", en: "Publisher" },
  dateOfPublication: { cs: "Datum vydání", en: "Date of publication" },
  language: { cs: "Jazyk", en: "Language" },
  degreeLabel: { cs: "Stupeň studia", en: "Degree" },
  degreePlaceholder: { cs: "Vyberte stupeň studia", en: "Select Degree" },
  specializationLabel: { cs: "Specializace", en: "Specialization" },
  specializationPlaceholder: { cs: "Vyberte specializaci", en: "Select Specialization" },
  nameLabel: { cs: "Název", en: "Name" },
  creditsLabel: { cs: "Počet kreditů", en: "Amount of credits" },
  descriptionLabel: { cs: "Popis", en: "Description" },
  modalHelpCreate: { cs: "Tento modál slouží pro vytvoření předmětu.", en: "Use this modal to create new subject." },
  modalHeaderCreate: { cs: "Vytvořit předmět", en: "Create Subject" },
  success: { cs: "Vytvoření předmětu proběhlo v pořádku.", en: "Create of subject was successfull." },
  fail: { cs: "Vytvoření předmětu selhalo.", en: "Create of subject failed." },
};

export default Lsi;
