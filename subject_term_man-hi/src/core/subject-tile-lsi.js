const Lsi = {
  stateLabel: { cs: "Stav", en: "State" },
  credits: { cs: "Počet kreditů", en: "Amount of credits" },
  subjectList: { cs: "List předmětů", en: "Subject list" },
  state: { cs: "Stav", en: "State" },
  header: { cs: "Předměty", en: "Subject" },
  code: { cs: "Kód", en: "Code" },
  updateButton: { cs: "Upravit", en: "Update" },
  deleteButton: { cs: "Smazat", en: "Delete" },
  createButton: { cs: "Vytvořit", en: "Create" },
  language: { cs: "Jazyk", en: "Language" },
  areYouSureToDelete: { cs: "Jste si jistý?", en: "Are your sure?" },
  cancel: { cs: "Zrušit", en: "Cancel" },
  delete: { cs: "Smazat", en: "Delete" },
  successDelete: { cs: "Smazaní proběhlo úspěšně", en: "Delete of book was successfull" },
  required: {
    cs: "Toto pole je povinné.",
    en: "This input is required.",
  },
};

export default Lsi;
