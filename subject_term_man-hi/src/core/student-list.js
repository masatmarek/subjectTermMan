//@@viewOn:imports
import * as UU5 from "uu5g04";
import "uu5g04-bricks";
import "uu5tilesg01";
import "uu_plus4u5g01-bricks";
import "uu_pg01-bricks";
import "uu_pg01-tiles";

import Config from "./config/config.js";
import Calls from "calls";

import CreateSubjectModal from "./create-subject-modal";
import StudentTile from "./student-tile";

//@@viewOff:imports

export const StudentList = UU5.Common.VisualComponent.create({
  //@@viewOn:mixins
  mixins: [UU5.Common.BaseMixin],
  //@@viewOff:mixins

  //@@viewOn:statics
  statics: {
    tagName: Config.TAG + "StudentList",
    classNames: {},
  },
  //@@viewOff:statics

  //@@viewOn:propTypes
  //@@viewOff:propTypes

  //@@viewOn:getDefaultProps
  //@@viewOff:getDefaultProps

  //@@viewOn:reactLifeCycle
  getInitialState() {
    this._listDataManager = UU5.Common.Reference.create();
    this._createModal = UU5.Common.Reference.create();
    this._updateModal = UU5.Common.Reference.create();
    return {};
  },
  //@@viewOff:reactLifeCycle

  //@@viewOn:interface
  //@@viewOff:interface

  //@@viewOn:overriding
  //@@viewOff:overriding

  //@@viewOn:private
  _handleLoad() {
    let data = {};
    let res = new Promise((done, fail) =>
      Calls.studentList({
        data,
        done: (data) => {
          done(data);
        },
        fail,
      })
    );
    return res;
  },
  _handleCreate(data) {
    return new Promise((resolve, reject) => {
      Calls.subjectCreate({
        data,
        done: (dtoOut) => {
          ModalHelper.close();
          resolve(dtoOut);
        },
        fail: (failDtoOut) => {
          UU5.Environment.getPage().getAlertBus().setAlert({ colorSchema: "danger", content: failDtoOut.message });
          ModalHelper.close();
          reject(failDtoOut);
        },
      });
    });
  },
  _openCreateModal() {
    this._createModal.current.open();
  },
  _getActionBarActions() {
    return [
      {
        content: "Create",
        onClick: this._openCreateModal,
        icon: "mdi-plus-circle",
        active: true,
      },
    ];
  },
  _renderTile(tileInfo) {
    return (
      <StudentTile key={tileInfo.code} data={tileInfo} onDelete={this._handleDelete} onUpdate={this._handleUpdate} />
    );
  },
  _getStudents() {
    return (
      <UU5.Common.ListDataManager ref_={this._listDataManager} onLoad={this._handleLoad} onCreate={this._handleCreate}>
        {({ viewState, errorState, errorData, data, handleLoad }) => {
          if (errorState) {
            // error
            return "Error";
          } else if (data) {
            // ready
            return (
              <div>
                <UU5.Tiles.ListController data={data} selectable={false}>
                  <UU5.Tiles.ActionBar
                    collapsible={false}
                    title={
                      <UU5.Bricks.Button bgStyle="transparent" onClick={() => UU5.Environment.setRoute("location")}>
                        <UU5.Bricks.Icon icon="mdi-arrow-left" />
                      </UU5.Bricks.Button>
                    }
                    searchable={true}
                    actions={this._getActionBarActions()}
                  />
                  <UU5.Tiles.List
                    tile={this._renderTile}
                    tileBorder
                    tileStyle={{ borderRadius: 4 }}
                    tileMinWidth={445}
                    tileHeight={200}
                    rowSpacing={8}
                    tileSpacing={8}
                    tileJustify="space-between"
                    scrollToAlignment="center"
                  />
                </UU5.Tiles.ListController>
                <CreateSubjectModal
                  onCreate={(newData) => {
                    console.log(this._listDataManager);
                    this._listDataManager.current.create({ ...data, ...newData });
                  }}
                  id={UU5.Common.Tools.generateUUID(6)}
                  ref_={this._createModal}
                />
              </div>
            );
          } else {
            // loading
            return <UU5.Bricks.Loading />;
          }
        }}
      </UU5.Common.ListDataManager>
    );
  },
  //@@viewOff:private

  //@@viewOn:render
  render() {
    return <UU5.Bricks.Div {...this.getMainPropsToPass()}>{this._getStudents()}</UU5.Bricks.Div>;
  },
  //@@viewOff:render
});

export default StudentList;
