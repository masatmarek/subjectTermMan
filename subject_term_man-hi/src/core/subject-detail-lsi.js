let Lsi = {
  credits: { cs: "Počet kreditů", en: "Amount of credits" },
  language: { cs: "Jazyk", en: "Language" },
  degree: { cs: "Stupeň studia", en: "Degree" },
  specialization: { cs: "Specializace", en: "Specialization" },
  addRuntime: { cs: "Přidat běh", en: "Add runtime" },
  "subject-term-man/subject/get/subjectNotExist": {
    cs: "Předmět neexistuje.",
    en: "Subject does not exist.",
  },
};

export default Lsi;
