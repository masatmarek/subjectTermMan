//@@viewOn:imports
import * as UU5 from "uu5g04";
import "uu5g04-bricks";
import "uu5tilesg01";
import "uu_plus4u5g01-bricks";
import "uu_pg01-bricks";
import "uu_pg01-tiles";

import Config from "./config/config.js";

import Lsi from "./subject-runtime-tile-lsi";

//@@viewOff:imports

export const SubjectRuntimeTile = UU5.Common.VisualComponent.create({
  //@@viewOn:mixins
  mixins: [UU5.Common.BaseMixin],
  //@@viewOff:mixins

  //@@viewOn:statics
  statics: {
    tagName: Config.TAG + "SubjectRuntimeTile",
    classNames: {
      main: () => Config.Css.css`
      border: 1px solid black;
      margin: 5px;
      padding: 10px;
      `,
      runtimeContainer: () => Config.Css.css`
      display: flex;
      justify-content: space-between;
      .tools {
        *{
            cursor: pointer;
            margin: 0 10px;
            font-size: 20px;
        }
        .edit{
            color: #4CAF50;
        }
        .delete{
            color: red;
        }
        .inspect{
            color: blue;
        }
      }
      `,
    },
  },
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    data: UU5.PropTypes.object.isRequired,
  },
  //@@viewOff:propTypes

  //@@viewOn:getDefaultProps
  //@@viewOff:getDefaultProps

  //@@viewOn:reactLifeCycle
  getInitialState() {
    this._listDataManager = UU5.Common.Reference.create();
    this._createModal = UU5.Common.Reference.create();
    this._updateModal = UU5.Common.Reference.create();
    return {};
  },
  //@@viewOff:reactLifeCycle

  //@@viewOn:interface
  //@@viewOff:interface

  //@@viewOn:overriding
  //@@viewOff:overriding

  //@@viewOn:private
  _getSubjectInfoLine(name, value) {
    return (
      <UU5.Bricks.Div key={name}>
        <strong>
          <UU5.Bricks.Lsi lsi={Lsi[name]} />
        </strong>
        :&nbsp;&nbsp;
        {typeof value !== "object" ? <UU5.Bricks.Span content={value} /> : <UU5.Bricks.Lsi lsi={value} />}
      </UU5.Bricks.Div>
    );
  },
  _getPropByCode(code, prop, name) {
    let object = UU5.Environment.App.termMan[prop];
    for (const iterator of object) {
      if (iterator.code === code) {
        return this._getSubjectInfoLine(name, iterator.name);
      }
    }
  },
  _getSubjectRuntime(runtime) {
    console.log(runtime);
    let classNames = this.getClassName();
    return (
      <div className={classNames.runtimeContainer()}>
        <UU5.Bricks.Lsi lsi={runtime.name} />
        <div className="tools">
          <UU5.Bricks.Icon className="edit" icon="mdi-pencil" />
          <UU5.Bricks.Icon className="delete" icon="mdi-delete" />
          <UU5.Bricks.Icon className="inspect" icon="mdi-eye" />
        </div>
      </div>
    );
  },
  //@@viewOff:private

  //@@viewOn:render
  render() {
    return <UU5.Bricks.Div {...this.getMainPropsToPass()}>{this._getSubjectRuntime(this.props.data)}</UU5.Bricks.Div>;
  },
  //@@viewOff:render
});

export default SubjectRuntimeTile;
