//@@viewOn:imports
import * as UU5 from "uu5g04";
import "uu5g04-bricks";
import "uu5tilesg01";
import "uu_pg01-bricks";
import "uu_pg01-tiles";

import Calls from "../calls";
import Config from "./config/config.js";

import Lsi from "./create-subject-modal-lsi.js";
//@@viewOff:imports

export const CreateSubjectModal = UU5.Common.VisualComponent.create({
  //@@viewOn:mixins
  mixins: [UU5.Common.BaseMixin, UU5.Common.ElementaryMixin],
  //@@viewOff:mixins

  //@@viewOn:statics
  statics: {
    tagName: Config.TAG + "CreateSubjectModal",
    classNames: {
      main: () => Config.Css.css``,
    },
  },
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    onCreate: UU5.PropTypes.func,
  },
  //@@viewOff:propTypes

  //@@viewOn:getDefaultProps
  getDefaultProps() {
    return { onCreate: null };
  },
  //@@viewOff:getDefaultProps

  //@@viewOn:reactLifeCycle
  getInitialState() {
    this._descriptionEditor = UU5.Common.Reference.create();
    this._modal = UU5.Common.Reference.create();
    this._alertBus = UU5.Common.Reference.create();
    return {};
  },
  //@@viewOff:reactLifeCycle

  //@@viewOn:interface
  open() {
    this._modal.current.open({
      header: this._getHeader(),
      content: this._getForm(),
      footer: this._getControls(),
      id: UU5.Common.Tools.generateUUID(6),
    });
  },
  //@@viewOff:interface

  //@@viewOn:overriding
  //@@viewOff:overriding

  //@@viewOn:private
  _getHeader() {
    return (
      <UU5.Forms.ContextHeader
        info={<UU5.Bricks.Lsi lsi={Lsi.modalHelpCreate} />}
        content={<UU5.Bricks.Lsi lsi={Lsi.modalHeaderCreate} />}
      />
    );
  },

  _handleCreate({ component, values }) {
    values.name = { cs: values.name };
    values.description = { cs: values.description };

    component.saveDone(values);
  },

  _handleCreateDone(dtoOut) {
    this._modal.current.close();
    this._alertBus.current.setAlert(
      {
        content: <UU5.Bricks.Div content={<UU5.Bricks.Lsi lsi={Lsi.success} />} />,
        colorSchema: "success",
      },
      () => this.props.onCreate && this.props.onCreate(dtoOut.dtoOut)
    );
  },

  _handleCreateFail() {
    this._alertBus.current.setAlert({
      content: <UU5.Bricks.Div content={<UU5.Bricks.Lsi lsi={Lsi.fail} />} />,
      colorSchema: "danger",
    });
  },
  _getItems(disereOptions) {
    let options = [];
    UU5.Environment.App.termMan[disereOptions].forEach((opt) => {
      options.push({ content: opt, value: opt });
    });
    return options;
  },
  _getOptions(disereOptions) {
    let options = [];
    UU5.Environment.App.termMan[disereOptions].forEach((opt, i) => {
      console.log(opt);
      options.push(<UU5.Forms.Select.Option key={i} value={opt.code} content={<UU5.Bricks.Lsi lsi={opt.name} />} />);
    });
    return options;
  },
  _getForm() {
    return (
      <UU5.Forms.ContextForm
        onSave={(formRef) => this._handleCreate(formRef)}
        onSaveDone={this._handleCreateDone}
        onSaveFail={this._handleCreateFail}
        onCancel={this._closeModal}
      >
        <UU5.Forms.Text
          name="name"
          labelColWidth={{ xs: 12 }}
          inputColWidth={{ xs: 12 }}
          label={<UU5.Bricks.Lsi lsi={Lsi.nameLabel} />}
          required
          requiredMessage={<UU5.Bricks.Lsi lsi={Lsi.required} />}
        />
        <UU5.Forms.TextArea
          name="description"
          label={<UU5.Bricks.Lsi lsi={Lsi.descriptionLabel} />}
          required
          requiredMessage={<UU5.Bricks.Lsi lsi={Lsi.required} />}
          size="m"
          labelColWidth={{ xs: 12 }}
          inputColWidth={{ xs: 12 }}
        />
        <UU5.Forms.SwitchSelector
          name="language"
          required
          requiredMessage={<UU5.Bricks.Lsi lsi={Lsi.required} />}
          label={<UU5.Bricks.Lsi lsi={Lsi.language} />}
          items={this._getItems("languages")}
          labelColWidth="xs-12"
          inputColWidth="xs-12"
        />
        <UU5.Forms.Select
          name="degreeCode"
          required
          requiredMessage={<UU5.Bricks.Lsi lsi={Lsi.required} />}
          label={<UU5.Bricks.Lsi lsi={Lsi.degreeLabel} />}
          placeholder={<UU5.Bricks.Lsi lsi={Lsi.degreePlaceholder} />}
        >
          {this._getOptions("degreeList")}
        </UU5.Forms.Select>
        <UU5.Forms.Select
          name="specializationCode"
          required
          requiredMessage={<UU5.Bricks.Lsi lsi={Lsi.required} />}
          label={<UU5.Bricks.Lsi lsi={Lsi.specializationLabel} />}
          placeholder={<UU5.Bricks.Lsi lsi={Lsi.specializationPlaceholder} />}
        >
          {this._getOptions("specializationList")}
        </UU5.Forms.Select>
        <UU5.Forms.Number
          name="credits"
          labelColWidth={{ xs: 12 }}
          inputColWidth={{ xs: 12 }}
          required
          requiredMessage={<UU5.Bricks.Lsi lsi={Lsi.required} />}
          min={0}
          label={<UU5.Bricks.Lsi lsi={Lsi.creditsLabel} />}
        />
      </UU5.Forms.ContextForm>
    );
  },

  _getControls() {
    return (
      <UU5.Forms.ContextControls
        buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.create} /> }}
        buttonCancelProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.cancel} /> }}
      />
    );
  },

  _closeModal() {
    this._modal.current.close();
  },
  //@@viewOff:private

  //@@viewOn:render
  render() {
    return (
      <UU5.Bricks.Div {...this.getMainPropsToPass()}>
        <UU5.Forms.ContextModal ref_={this._modal} overflow={true} />
        <UU5.Bricks.AlertBus ref_={this._alertBus} location="portal" />
      </UU5.Bricks.Div>
    );
  },
  //@@viewOff:render
});
export default CreateSubjectModal;
