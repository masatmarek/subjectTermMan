//@@viewOn:imports
import * as UU5 from "uu5g04";
import "uu5g04-bricks";
import "uu5tilesg01";
import "uu_plus4u5g01-bricks";
import "uu_pg01-bricks";
import "uu_pg01-tiles";

import Config from "./config/config.js";
import Calls from "calls";

import SubjectRuntimeTile from "./subject-runtime-tile";

import Lsi from "./subject-detail-lsi.js";

//@@viewOff:imports

export const SubjectDetail = UU5.Common.VisualComponent.create({
  //@@viewOn:mixins
  mixins: [UU5.Common.BaseMixin],
  //@@viewOff:mixins

  //@@viewOn:statics
  statics: {
    tagName: Config.TAG + "SubjectDetail",
    classNames: {
      main: () => Config.Css.css`
      margin: 0 5vw;
    `,
    addRuntimeBtn: () => Config.Css.css`
      float: right;
    `
    },
  },
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    id: UU5.PropTypes.string.isRequired,
  },
  //@@viewOff:propTypes

  //@@viewOn:getDefaultProps
  //@@viewOff:getDefaultProps

  //@@viewOn:reactLifeCycle
  getInitialState() {
    this._listDataManager = UU5.Common.Reference.create();
    this._createModal = UU5.Common.Reference.create();
    this._updateModal = UU5.Common.Reference.create();
    return {};
  },
  //@@viewOff:reactLifeCycle

  //@@viewOn:interface
  //@@viewOff:interface

  //@@viewOn:overriding
  //@@viewOff:overriding

  //@@viewOn:private
  _handleLoad() {
    let data = {};
    let res = new Promise((done, fail) =>
      Calls.subjectList({
        data,
        done: (data) => {
          done(data);
        },
        fail,
      })
    );
    return res;
  },
  _handleCreate(data) {
    return new Promise((resolve, reject) => {
      Calls.subjectCreate({
        data,
        done: (dtoOut) => {
          ModalHelper.close();
          resolve(dtoOut);
        },
        fail: (failDtoOut) => {
          UU5.Environment.getPage().getAlertBus().setAlert({ colorSchema: "danger", content: failDtoOut.message });
          ModalHelper.close();
          reject(failDtoOut);
        },
      });
    });
  },
  _openCreateModal() {
    this._createModal.current.open();
  },
  _getSubjectInfoLine(name, value) {
    return (
      <UU5.Bricks.Div key={name}>
        <strong>
          <UU5.Bricks.Lsi lsi={Lsi[name]} />
        </strong>
        :&nbsp;&nbsp;
        {typeof value !== "object" ? <UU5.Bricks.Span content={value} /> : <UU5.Bricks.Lsi lsi={value} />}
      </UU5.Bricks.Div>
    );
  },
  _getPropByCode(code, prop, name) {
    let object = UU5.Environment.App.termMan[prop];
    for (const iterator of object) {
      if (iterator.code === code) {
        return this._getSubjectInfoLine(name, iterator.name);
      }
    }
  },
  _getRuntimes(data) {
    let tiles = [];
    if (data && data.length) {
      data.forEach((element) => {
        tiles.push(<SubjectRuntimeTile data={element} />);
      });
    }
    else{
      tiles = "Předmět zatím neobsahuje žádné běhy"
    }
    let result = <div>
      {tiles}
      <UU5.Bricks.Button className={this.getClassName().addRuntimeBtn()} colorSchema="success" content={<UU5.Bricks.Lsi lsi={Lsi.addRuntime}/>}/>
    </div>
    return result;
  },
  _getSubject() {
    return (
      <UU5.Common.Loader onLoad={Calls.getSubject} data={{ id: this.props.id }}>
        {({ isLoading, isError, data }) => {
          if (isLoading) {
            return <UU5.Bricks.Loading />;
          } else if (isError) {
            if (data.code === "subject-term-man/subject/get/subjectNotExist") {
              return (
                <>
                  <UU5.Bricks.Button bgStyle="transparent" onClick={() => UU5.Environment.setRoute("subject")}>
                    <UU5.Bricks.Icon icon="mdi-arrow-left" />
                  </UU5.Bricks.Button>
                  <div>
                    <UU5.Bricks.Lsi lsi={Lsi[data.code]} />
                  </div>
                </>
              );
            } else {
              return <Error data={data} />;
            }
          } else {
            return (
              <>
                <UU5.Bricks.Button bgStyle="transparent" onClick={() => UU5.Environment.setRoute("subject")}>
                  <UU5.Bricks.Icon icon="mdi-arrow-left" />
                </UU5.Bricks.Button>
                <div>
                  <h2>
                    <UU5.Bricks.Lsi lsi={data.name} />
                  </h2>
                  {<UU5.Bricks.Lsi lsi={data.description} />}
                  {this._getSubjectInfoLine("credits", data.credits)}
                  {this._getSubjectInfoLine("language", data.language)}
                  {this._getPropByCode(data.degreeCode, "degreeList", "degree")}
                  {this._getPropByCode(data.specializationCode, "specializationList", "specialization")}
                </div>
                <h4>Běhy předmětu:</h4>
                {this._getRuntimes(data.subjectRuntimeList)}
              </>
            );
          }
        }}
      </UU5.Common.Loader>
    );
  },
  //@@viewOff:private

  //@@viewOn:render
  render() {
    return <UU5.Bricks.Div {...this.getMainPropsToPass()}>{this._getSubject()}</UU5.Bricks.Div>;
  },
  //@@viewOff:render
});

export default SubjectDetail;
