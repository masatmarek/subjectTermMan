const Lsi = {
  stateLabel: { cs: "Stav", en: "State" },
  firstname: { cs: "Jméno", en: "Firstname" },
  lastname: { cs: "Příjmení", en: "Lastname" },
  language: { cs: "Jazyk", en: "Language" },
  areYouSureToDelete: { cs: "Jste si jistý?", en: "Are your sure?" },
  cancel: { cs: "Zrušit", en: "Cancel" },
  delete: { cs: "Smazat", en: "Delete" },
  successDelete: { cs: "Smazaní proběhlo úspěšně", en: "Delete of book was successfull" },
  required: {
    cs: "Toto pole je povinné.",
    en: "This input is required.",
  },
};

export default Lsi;
