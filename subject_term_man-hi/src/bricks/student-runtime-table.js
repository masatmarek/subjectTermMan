//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";

import Config from "./config/config.js";
//@@viewOff:imports

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "WelcomeRow",
  //@@viewOff:statics
};

const CLASS_NAMES = {
};

export const StudentRuntimeTable = UU5.Common.VisualComponent.create({
  render() {
    let props = {
      className: 'dt2',
      header: 'Zapsané předměty',
      //footer: "Footer - Content",
      footer: {
        element: {
          tag: "UU5.Bricks.Footer",
        },
      },
      headerRow: [
        'Předmět',
        'Body',
        'Známka',
      ],
      rows: [
        [
          'Matematika (zima 2020)',
          '25',
          '4',
        ],
        [
          'Software (zima 2020)',
          '60',
          '3',
        ],
        [
          'Cloudové aplikace (zima 2020)',
          '60',
          '3',
        ],
      ],
    };
    return (
      <UU5.Bricks.Container>
        {/*@@viewOn:0*/}
        <UU5.Bricks.DataTable
          striped
          bordered
          hover
          condensed
          {...props}
        />
        {/*@@viewOff:0*/}
      </UU5.Bricks.Container>
    );
  }
});

export default StudentRuntimeTable;
