"use strict";
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory, ObjectStoreError } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const { Profile, AppClientTokenService, UuAppWorkspace, UuAppWorkspaceError } = require("uu_appg01_server").Workspace;
const { UriBuilder } = require("uu_appg01_server").Uri;
const { LoggerFactory } = require("uu_appg01_server").Logging;
const { AppClient } = require("uu_appg01_server");
const Errors = require("../api/errors/term-man-error.js");

const Helper = require("../helpers/custom-helper");

const WARNINGS = {
  initUnsupportedKeys: {
    code: `${Errors.Init.UC_CODE}unsupportedKeys`,
  },
  createUnsupportedKeys: {
    code: `${Errors.Create.UC_CODE}unsupportedKeys`,
  },
  getUnsupportedKeys: {
    code: `${Errors.Get.UC_CODE}unsupportedKeys`,
  },
};

const logger = LoggerFactory.get("TermManAbl");

class TermManAbl {
  constructor() {
    this.validator = Validator.load();
    this.dao = DaoFactory.getDao("termMan");
  }

  async get(awid, dtoIn) {
    // HDS 1
    let validationResult = this.validator.validate("getDtoInType", dtoIn);
    // A1, A2
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.getUnsupportedKeys.code,
      Errors.Get.InvalidDtoIn
    );
    // HDS 2
    let dtoOut = await this.dao.getByAwid(awid);
    if (!dtoOut) {
      // A3
      throw new Errors.Get.SubjectTermManDoesNotExist();
    }
    //HDS 3
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async create(awid, dtoIn, session) {
    // HDS 1
    let workspace = await UuAppWorkspace.get(awid);
    let uuBtUriBuilder = UriBuilder.parse(workspace.artifactUri);
    let awscId = uuBtUriBuilder.toUri().parameters.id;
    let awscLoadUri = uuBtUriBuilder.setUseCase("uuAwsc/load").clearParameters().toUri();

    try {
      awscResponse = await AppClient.get(awscLoadUri, { id: awscId }, { session });
    } catch (error) {
      // A1, A2, A3
      if (error.code === "uu-appg01/authorization/accessDenied") {
        throw new Errors.Create.AccessDenied({ ...error });
      } else if (error.code === "uu-businessterritory-maing01/authorization/artifactDoesNotExist") {
        throw new Errors.Create.ArtifactDoesNotExis({ ...error });
      } else {
        throw new Errors.Create.UuBtLoadFailed({ ...error });
      }
    }
    // HDS 2
    let validationResult = this.validator.validate("createDtoInType", dtoIn);
    // A4, A5
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.createUnsupportedKeys.code,
      Errors.Create.InvalidDtoIn
    );

    // HDS 3
    dtoIn.awid = awid;
    dtoIn && dtoIn.state ? null : (dtoIn.state = "active");

    // HDS 4
    dtoIn.degreeList = Helper.transformValuesIntoObject(dtoIn.degreeList);
    dtoIn.specializationList = Helper.transformValuesIntoObject(dtoIn.specializationList);
    dtoIn.studyFormList = Helper.transformValuesIntoObject(dtoIn.studyFormList);

    // HDS 5
    let dtoOut = {};
    try {
      dtoOut = await this.dao.create(dtoIn);
    } catch (error) {
      throw new Errors.Create.CreateByDaoFailed({ ...error });
    }

    // HDS 6
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async load(awid, session) {
    let workspace = await UuAppWorkspace.get(awid);

    let uuBtUriBuilder = UriBuilder.parse(workspace.artifactUri);
    let awscId = uuBtUriBuilder.toUri().parameters.id;
    let awscLoadUri = uuBtUriBuilder.setUseCase("uuAwsc/load").clearParameters().toUri();
    let awscResponse;
    try {
      awscResponse = await AppClient.get(awscLoadUri, { id: awscId }, { session });
    } catch (error) {
      if (error.code === "uu-appg01/authorization/accessDenied") {
        throw new Errors.Load.AccessDenied({ ...error });
      } else if (error.code === "uu-businessterritory-maing01/authorization/artifactDoesNotExist") {
        throw new Errors.Load.ArtifactDoesNotExis({ ...error });
      } else {
        throw new Errors.Load.UuBtLoadFailed({ ...error });
      }
    }

    return {
      awsc: awscResponse.data,
    };
  }

  async init(uri, dtoIn, session) {
    const awid = uri.getAwid();
    // HDS 1
    let validationResult = this.validator.validate("initDtoInType", dtoIn);
    // A1, A2
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.initUnsupportedKeys.code,
      Errors.Init.InvalidDtoIn
    );

    // HDS 2
    const schemas = ["termMan"];
    let schemaCreateResults = schemas.map(async (schema) => {
      try {
        return await DaoFactory.getDao(schema).createSchema();
      } catch (e) {
        // A3
        throw new Errors.Init.SchemaDaoCreateSchemaFailed({ uuAppErrorMap }, { schema }, e);
      }
    });
    await Promise.all(schemaCreateResults);

    if (dtoIn.uuBtLocationUri) {
      const baseUri = uri.getBaseUri();
      const uuBtUriBuilder = UriBuilder.parse(dtoIn.uuBtLocationUri);
      const location = uuBtUriBuilder.getParameters().id;
      const uuBtBaseUri = uuBtUriBuilder.toUri().getBaseUri();

      const createAwscDtoIn = {
        name: "SubjectTerm",
        typeCode: "subject-term-man",
        location: location,
        uuAppWorkspaceUri: baseUri,
      };

      const awscCreateUri = uuBtUriBuilder.setUseCase("uuAwsc/create").toUri();
      const appClientToken = await AppClientTokenService.createToken(uri, uuBtBaseUri);
      const callOpts = AppClientTokenService.setToken({ session }, appClientToken);

      // TODO HDS
      let awscId;
      try {
        const awscDtoOut = await AppClient.post(awscCreateUri, createAwscDtoIn, callOpts);
        awscId = awscDtoOut.id;
      } catch (e) {
        if (e.code.includes("applicationIsAlreadyConnected") && e.paramMap.id) {
          logger.warn(`Awsc already exists id=${e.paramMap.id}.`, e);
          awscId = e.paramMap.id;
        } else {
          throw new Errors.Init.CreateAwscFailed({ uuAppErrorMap }, { location: dtoIn.uuBtLocationUri }, e);
        }
      }

      const artifactUri = uuBtUriBuilder.setUseCase(null).clearParameters().setParameter("id", awscId).toUri();

      await UuAppWorkspace.connectArtifact(
        baseUri,
        {
          artifactUri: artifactUri.toString(),
          synchronizeArtifactBasicAttributes: false,
        },
        session
      );
    }

    // HDS 3
    if (dtoIn.uuAppProfileAuthorities) {
      try {
        await Profile.set(awid, "Authorities", dtoIn.uuAppProfileAuthorities);
      } catch (e) {
        if (e instanceof UuAppWorkspaceError) {
          // A4
          throw new Errors.Init.SysSetProfileFailed({ uuAppErrorMap }, { role: dtoIn.uuAppProfileAuthorities }, e);
        }
        throw e;
      }
    }

    // HDS 4 - HDS N
    // TODO Implement according to application needs...

    // HDS N+1
    const workspace = UuAppWorkspace.get(awid);

    return {
      ...workspace,
      uuAppErrorMap: uuAppErrorMap,
    };
  }
}

module.exports = new TermManAbl();
