"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/subject-term-man-main-error.js");

const WARNINGS = {

};

class SubjectTermManMainAbl {

  constructor() {
    this.validator = Validator.load();
    // this.dao = DaoFactory.getDao("subjectTermManMain");
  }

  async load(awid, dtoIn) {
    
  }

}

module.exports = new SubjectTermManMainAbl();
