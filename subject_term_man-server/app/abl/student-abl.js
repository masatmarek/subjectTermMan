"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/student-error.js");
const Helper = require("../helpers/custom-helper");

const WARNINGS = {
  createUnsupportedKeys: {
    code: `${Errors.Create.UC_CODE}unsupportedKeys`,
  },
  listUnsupportedKeys: {
    code: `${Errors.List.UC_CODE}unsupportedKeys`,
  },

};

const TermManStates = {
  active: "active",
  suspend: "suspend",
  underConstruction: "underConstruction",
  closed: "closed",
};

class StudentAbl {

  constructor() {
    this.validator = Validator.load();
    this.dao = DaoFactory.getDao("student");
    this.daoTermMan = DaoFactory.getDao("termMan");
  }

  async create(awid, dtoIn) {


    //check if uuBt is valid
    let subjectTermMan = await this.daoTermMan.getByAwid(awid);
    if (!subjectTermMan) {
   
      throw new Errors.Create.SubjectTermManDoesNotExist();
    }
    if (Helper.checkTermManAllowedStates(subjectTermMan.state)) {
  
      throw new Errors.Create.SubjectTermManIsNotInCorrectState({
        subjectTermMan: { id: subjectTermMan.id, name: subjectTermMan.name },
        state: subjectTermMan.state,
        excpectedStates: [TermManStates.active, TermManStates.underConstruction],
      });
    }

 
    let validationResult = this.validator.validate("createStudentDtoInType", dtoIn);

    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.createUnsupportedKeys.code,
      Errors.Create.InvalidDtoIn
    );


    dtoIn.awid = awid;
    dtoIn && dtoIn.state ? null : (dtoIn.state = "active");

    let dtoOut;
    

    try {
      dtoOut = await this.dao.create(dtoIn);
    } catch (error) {
      throw new Errors.Create.CreateByDaoFailed({ ...error });
    }

    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;


  }

  async list(awid, dtoIn) {

    
    // HDS 2
    let subjectTermMan = await this.daoTermMan.getByAwid(awid);
    if (!subjectTermMan) {
      // A4
      throw new Errors.List.SubjectTermManDoesNotExist();
    }
    if (Helper.checkTermManAllowedStates(subjectTermMan.state)) {
      // A5
      throw new Errors.List.SubjectTermManIsNotInCorrectState({
        subjectTermMan: { id: subjectTermMan.id, name: subjectTermMan.name },
        state: subjectTermMan.state,
        excpectedStates: [TermManStates.active, TermManStates.underConstruction],
      });
    }

    // HDS 3
    let validationResult = this.validator.validate("listStudentDtoInType", dtoIn);
    // A6, A7
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.listUnsupportedKeys.code,
      Errors.List.InvalidDtoIn
    );
    // HDS 4
    dtoIn.awid = awid;
    let dtoOut = await this.dao.listByCriteria(awid, dtoIn.pageInfo);
    // HDS 6
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

}

module.exports = new StudentAbl();
