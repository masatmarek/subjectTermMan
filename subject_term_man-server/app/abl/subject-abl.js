"use strict";
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory, ObjectStoreError } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const { Profile, AppClientTokenService, UuAppWorkspace, UuAppWorkspaceError } = require("uu_appg01_server").Workspace;
const { UriBuilder } = require("uu_appg01_server").Uri;
const { LoggerFactory } = require("uu_appg01_server").Logging;
const { AppClient } = require("uu_appg01_server");
const Errors = require("../api/errors/subject-error.js");
const Helper = require("../helpers/custom-helper");

const WARNINGS = {
  createUnsupportedKeys: {
    code: `${Errors.Create.UC_CODE}unsupportedKeys`,
  },
  listUnsupportedKeys: {
    code: `${Errors.List.UC_CODE}unsupportedKeys`,
  },
  getUnsupportedKeys: {
    code: `${Errors.Get.UC_CODE}unsupportedKeys`,
  },
};
const TermManStates = {
  active: "active",
  suspend: "suspend",
  underConstruction: "underConstruction",
  closed: "closed",
};
const SubjectStates = {
  active: "active",
  passive: "passive",
};
class SubjectAbl {
  constructor() {
    this.validator = Validator.load();
    this.dao = DaoFactory.getDao("subject");
    this.daoTermMan = DaoFactory.getDao("termMan");
  }
  async get(awid, dtoIn) {
    // HDS 2
    let subjectTermMan = await this.daoTermMan.getByAwid(awid);
    if (!subjectTermMan) {
      // A4
      throw new Errors.Get.SubjectTermManDoesNotExist();
    }
    if (Helper.checkTermManAllowedStates(subjectTermMan.state)) {
      // A5
      throw new Errors.Get.SubjectTermManIsNotInCorrectState({
        subjectTermMan: { id: subjectTermMan.id, name: subjectTermMan.name },
        state: subjectTermMan.state,
        excpectedStates: [TermManStates.active, TermManStates.underConstruction],
      });
    }

    // HDS 3
    let validationResult = this.validator.validate("getSubjectDtoInType", dtoIn);
    // A6, A7
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.getUnsupportedKeys.code,
      Errors.Get.InvalidDtoIn
    );

    //HDS 4
    let dtoOut = await this.dao.get(awid, dtoIn.id);
    if (!dtoOut) {
      throw new Errors.Get.SubjectDoesNotExist({ uuAppErrorMap });
    }

    //HDS 5
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }
  async list(awid, dtoIn) {
    // HDS 2
    let subjectTermMan = await this.daoTermMan.getByAwid(awid);
    if (!subjectTermMan) {
      // A4
      throw new Errors.List.SubjectTermManDoesNotExist();
    }
    if (Helper.checkTermManAllowedStates(subjectTermMan.state)) {
      // A5
      throw new Errors.List.SubjectTermManIsNotInCorrectState({
        subjectTermMan: { id: subjectTermMan.id, name: subjectTermMan.name },
        state: subjectTermMan.state,
        excpectedStates: [TermManStates.active, TermManStates.underConstruction],
      });
    }

    // HDS 3
    let validationResult = this.validator.validate("listSubjectDtoInType", dtoIn);
    // A6, A7
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.listUnsupportedKeys.code,
      Errors.List.InvalidDtoIn
    );
    // HDS 4
    dtoIn.awid = awid;
    let dtoOut = await this.dao.listByCriteria(awid, dtoIn.pageInfo);
    // HDS 6
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async create(awid, dtoIn, session) {
    // HDS 1
    // let workspace = await UuAppWorkspace.get(awid);
    // let uuBtUriBuilder = UriBuilder.parse(workspace.artifactUri);
    // let awscId = uuBtUriBuilder.toUri().parameters.id;
    // let awscLoadUri = uuBtUriBuilder.setUseCase("uuAwsc/load").clearParameters().toUri();
    // console.log(awscLoadUri);
    // try {
    //   awscResponse = await AppClient.get(awscLoadUri, { id: awscId }, { session });
    // } catch (error) {
    //   // A1, A2, A3
    //   if (error.code === "uu-appg01/authorization/accessDenied") {
    //     throw new Errors.Create.AccessDenied({ ...error });
    //   } else if (error.code === "uu-businessterritory-maing01/authorization/artifactDoesNotExist") {
    //     throw new Errors.Create.ArtifactDoesNotExis({ ...error });
    //   } else {
    //     throw new Errors.Create.UuBtLoadFailed({ error });
    //   }
    // }

    // HDS 2
    let subjectTermMan = await this.daoTermMan.getByAwid(awid);
    if (!subjectTermMan) {
      // A4
      throw new Errors.Create.SubjectTermManDoesNotExist();
    }
    if (Helper.checkTermManAllowedStates(subjectTermMan.state)) {
      // A5
      throw new Errors.Create.SubjectTermManIsNotInCorrectState({
        subjectTermMan: { id: subjectTermMan.id, name: subjectTermMan.name },
        state: subjectTermMan.state,
        excpectedStates: [TermManStates.active, TermManStates.underConstruction],
      });
    }

    // HDS 3
    let validationResult = this.validator.validate("createSubjectDtoInType", dtoIn);
    // A6, A7
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.createUnsupportedKeys.code,
      Errors.Create.InvalidDtoIn
    );
    // HDS 4
    let degreeCodeIsValid = false;
    for (const element of subjectTermMan.degreeList) {
      if (element.code === dtoIn.degreeCode) {
        degreeCodeIsValid = true;
        break;
      }
    }
    if (!degreeCodeIsValid) {
      // A8
      throw new Errors.Create.DegreeCodeDoesNotExists({ degreeCode: dtoIn.degreeCode });
    }
    let specializationCodeIsValid = false;
    for (const element of subjectTermMan.specializationList) {
      if (element.code === dtoIn.specializationCode) {
        specializationCodeIsValid = true;
        break;
      }
    }
    if (!specializationCodeIsValid) {
      // A8
      throw new Errors.Create.SpecializationCodeDoesNotExists({ specializationCode: dtoIn.specializationCode });
    }

    // HDS 5
    dtoIn.awid = awid;
    dtoIn && dtoIn.state ? null : (dtoIn.state = "active");
    dtoIn.subjectRuntimeList = [];
    // HDS 6
    let dtoOut;
    try {
      dtoOut = await this.dao.create(dtoIn);
    } catch (error) {
      throw new Errors.Create.CreateByDaoFailed({ ...error });
    }
    // HDS 7
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }
}

module.exports = new SubjectAbl();
