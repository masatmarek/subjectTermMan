const Helper = {
    generateCode: (name) => {
      let code = "";
      for (let i = 0; i < name.length; i++) {
        const inSymbol = name[i];
        let outSymbol;
        switch (inSymbol.toLowerCase()) {
          case "á":
            outSymbol = "a";
            break;
          case "é":
            outSymbol = "e";
            break;
          case "ě":
            outSymbol = "e";
            break;
          case "í":
            outSymbol = "i";
            break;
          case "ú":
            outSymbol = "u";
            break;
          case "ů":
            outSymbol = "u";
            break;
          case "č":
            outSymbol = "c";
            break;
          case "ó":
            outSymbol = "o";
            break;
          case "š":
            outSymbol = "s";
            break;
          case "ť":
            outSymbol = "t";
            break;
          case "ř":
            outSymbol = "r";
            break;
          case "ž":
            outSymbol = "z";
            break;
          case "ý":
            outSymbol = "y";
            break;
          case "ň":
            outSymbol = "n";
            break;
          case "ď":
            outSymbol = "d";
            break;
          case " ":
            outSymbol = "-";
            break;
          case ".":
            outSymbol = "";
            break;
          default:
            outSymbol = inSymbol;
            break;
        }
        code += outSymbol.toLowerCase();
      }
      return code;
    },
    transformValuesIntoObject: (array) => {
      let newArray = [];
      array.forEach((element) => {
        let code = element.hasOwnProperty("en") ? element["en"] : element[Object.keys(element)[0]];
        newArray.push({ code: module.exports.generateCode(code), name: element });
      });
      return newArray;
    },
    checkTermManAllowedStates: (state) => {
      let allowedStates = ["active", "underConstruction"];
      return !allowedStates.includes(state);
    },
  };
  
  module.exports = Helper;
  