/* eslint-disable */

const initDtoInType = shape({
  uuAppProfileAuthorities: uri().isRequired("uuBtLocationUri"),
  uuBtLocationUri: uri(),
  name: uu5String(512),
  sysState: oneOf(["active", "restricted", "readOnly"]),
  adviceNote: shape({
    message: uu5String().isRequired(),
    severity: oneOf(["debug", "info", "warning", "error", "fatal"]),
    estimatedEndTime: datetime(),
  }),
});

const createDtoInType = shape({
  name: uu5String(512),
  degreeList: array(shape({})).isRequired(),
  specializationList: array().isRequired(),
  studyFormList: array().isRequired(),
  grades: shape().isRequired(),
  languages: array().isRequired(),
  primaryLanguage: string().isRequired(),
  state: oneOf(["active", "suspend", "underConstruction", "closed"]),
});
const getDtoInType = shape({});
