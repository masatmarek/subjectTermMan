const createStudentDtoInType = shape({
    firstname: string().isRequired(),
    lastname: string().isRequired(),
    specialization: string().isRequired(),
    studyForm: string().isRequired(),
    language: string().isRequired()
  })

  const listStudentDtoInType = shape({
    pageInfo: shape({
      pageIndex: integer(),
      pageSize: integer(),
    }),
  });