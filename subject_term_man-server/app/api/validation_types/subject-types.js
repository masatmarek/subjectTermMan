/* eslint-disable */
const createSubjectDtoInType = shape({
  name: shape().isRequired(),
  credits: number().isRequired(),
  description: shape(),
  language: string().isRequired(),
  degreeCode: string().isRequired(),
  specializationCode: string().isRequired(),
  state: oneOf(["active", "passive"]),
});

const listSubjectDtoInType = shape({
  pageInfo: shape({
    pageIndex: integer(),
    pageSize: integer(),
  }),
});
const getSubjectDtoInType = shape({
  id: id().isRequired()
});
