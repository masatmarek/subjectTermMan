"use strict";
const TermManAbl = require("../../abl/term-man-abl.js");

class TermManController {

  get(ucEnv) {
    return TermManAbl.get(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
  create(ucEnv) {
    return TermManAbl.create(ucEnv.getUri().getAwid(), ucEnv.getDtoIn(), ucEnv.getSession());
  }

  load(ucEnv) {
    return TermManAbl.load(ucEnv.getUri().getAwid(), ucEnv.getSession());
  }
  init(ucEnv) {
    return TermManAbl.init(ucEnv.getUri(), ucEnv.getDtoIn(), ucEnv.getSession());
  }
}

module.exports = new TermManController();
