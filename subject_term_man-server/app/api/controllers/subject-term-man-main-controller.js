"use strict";
const SubjectTermManMainAbl = require("../../abl/subject-term-man-main-abl.js");

class SubjectTermManMainController {

  load(ucEnv) {
    return SubjectTermManMainAbl.load(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

}

module.exports = new SubjectTermManMainController();
