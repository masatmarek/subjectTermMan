"use strict";

const TermManUseCaseError = require("./term-man-use-case-error.js");
const SUBJECT_ERROR_PREFIX = `${TermManUseCaseError.ERROR_PREFIX}subject/`;

const Create = {
  UC_CODE: `${SUBJECT_ERROR_PREFIX}create/`,
  AccessDenied:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}accessDenied`;
      this.message = "Access to uuBt was denied.";
    }
  },
  ArtifactDoesNotExis:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}artifactDoesNotExis`;
      this.message = "Artifact does not exis.";
    }
  },
  UuBtLoadFailed:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}uuBtLoadFailed`;
      this.message = "Load data from uuBusinessTerritory failed.";
    }
  },
  InvalidDtoIn: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  DegreeCodeDoesNotExists: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}degreeCodeDoesNotExists`;
      this.message = "DegreeCode does not exists.";
    }
  },
  SpecializationCodeDoesNotExists: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}specializationCodeDoesNotExists`;
      this.message = "SpecializationCode does not exists.";
    }
  },
  CreateByDaoFailed: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.status = 500;
      this.code = `${Create.UC_CODE}createByDaoFailed`;
      this.message = "Create of Subject failed.";
    }
  },
  SubjectTermManDoesNotExist: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}subjectTermManDoesNotExist`;
      this.message = "SubjectTermMan does not exist";
    }
  },
  SubjectTermManIsNotInCorrectState: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}subjectTermManIsNotInCorrectState`;
      this.message = "SubjectTermMan is not in correct state.";
    }
  },
};


const List = {
  UC_CODE: `${SUBJECT_ERROR_PREFIX}list/`,
  
  InvalidDtoIn: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  SubjectTermManDoesNotExist: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}subjectTermManDoesNotExist`;
      this.message = "SubjectTermMan does not exist";
    }
  },
  SubjectTermManIsNotInCorrectState: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}subjectTermManIsNotInCorrectState`;
      this.message = "SubjectTermMan is not in correct state.";
    }
  },
};

const Get = {
  UC_CODE: `${SUBJECT_ERROR_PREFIX}get/`,
  InvalidDtoIn: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  SubjectTermManDoesNotExist: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}subjectTermManDoesNotExist`;
      this.message = "SubjectTermMan does not exist";
    }
  },
  SubjectTermManIsNotInCorrectState: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}subjectTermManIsNotInCorrectState`;
      this.message = "SubjectTermMan is not in correct state.";
    }
  },
  SubjectDoesNotExist: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}subjectNotExist`;
      this.message = "Subject does not exist";
    }
  },
};

module.exports = {
  Get,
  List,
  Create,
};
