"use strict";
const TermManUseCaseError = require("./term-man-use-case-error.js");

const Init = {
  UC_CODE: `${TermManUseCaseError.ERROR_PREFIX}init/`,

  InvalidDtoIn: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Init.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },

  SchemaDaoCreateSchemaFailed: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.status = 500;
      this.code = `${Init.UC_CODE}schemaDaoCreateSchemaFailed`;
      this.message = "Create schema by Dao createSchema failed.";
    }
  },

  SysSetProfileFailed: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Init.UC_CODE}sys/setProfileFailed`;
      this.message = "Set profile failed.";
    }
  },

  CreateAwscFailed: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Init.UC_CODE}createAwscFailed`;
      this.message = "Create uuAwsc failed.";
    }
  },
};

const Load = {
  UC_CODE: `${TermManUseCaseError.ERROR_PREFIX}load/`,
  AccessDenied:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Load.UC_CODE}accessDenied`;
      this.message = "Access to uuBt was denied.";
    }
  },
  ArtifactDoesNotExis:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Load.UC_CODE}artifactDoesNotExis`;
      this.message = "Artifact does not exis.";
    }
  },
  UuBtLoadFailed:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Load.UC_CODE}uuBtLoadFailed`;
      this.message = "Load data from uuBusinessTerritory failed.";
    }
  },
};

const Create = {
  UC_CODE: `${TermManUseCaseError.ERROR_PREFIX}create/`,
  InvalidDtoIn: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  AccessDenied:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}accessDenied`;
      this.message = "Access to uuBt was denied.";
    }
  },
  ArtifactDoesNotExis:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}artifactDoesNotExis`;
      this.message = "Artifact does not exis.";
    }
  },
  UuBtLoadFailed:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}uuBtLoadFailed`;
      this.message = "Load data from uuBusinessTerritory failed.";
    }
  },
  CreateByDaoFailed: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}createByDaoFailed`;
      this.message = "Create SubjectTermMan by dao failed.";
    }
  },
};

const Get = {
  UC_CODE: `${TermManUseCaseError.ERROR_PREFIX}get/`,
  InvalidDtoIn: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  SubjectTermManDoesNotExist: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Get.UC_CODE}subjectTermManDoesNotExist`;
      this.message = "SubjectTermMan does not exist";
    }
  },
  
};

module.exports = {
  Get,
  Create,
  Load,
  Init,
};
