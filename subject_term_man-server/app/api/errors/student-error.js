"use strict";

const TermManUseCaseError = require("./term-man-use-case-error.js");
const STUDENT_ERROR_PREFIX = `${TermManUseCaseError.ERROR_PREFIX}student/`;

const Create = {
  UC_CODE: `${STUDENT_ERROR_PREFIX}create/`,

  AccessDenied:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}accessDenied`;
      this.message = "Access to uuBt was denied.";
    }
  },
  ArtifactDoesNotExis:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}artifactDoesNotExis`;
      this.message = "Artifact does not exis.";
    }
  },
  UuBtLoadFailed:class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}uuBtLoadFailed`;
      this.message = "Load data from uuBusinessTerritory failed.";
    }
  },
  InvalidDtoIn: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  DegreeCodeDoesNotExists: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}degreeCodeDoesNotExists`;
      this.message = "DegreeCode does not exists.";
    }
  },
  CreateByDaoFailed: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.status = 500;
      this.code = `${Create.UC_CODE}createByDaoFailed`;
      this.message = "Create of Subject failed.";
    }
  },
  SubjectTermManDoesNotExist: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}subjectTermManDoesNotExist`;
      this.message = "SubjectTermMan does not exist";
    }
  },
  SubjectTermManIsNotInCorrectState: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}subjectTermManIsNotInCorrectState`;
      this.message = "SubjectTermMan is not in correct state.";
    }
  },
  
};

const List = {
  UC_CODE: `${STUDENT_ERROR_PREFIX}list/`,
  
  InvalidDtoIn: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  SubjectTermManDoesNotExist: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}subjectTermManDoesNotExist`;
      this.message = "SubjectTermMan does not exist";
    }
  },
  SubjectTermManIsNotInCorrectState: class extends TermManUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}subjectTermManIsNotInCorrectState`;
      this.message = "SubjectTermMan is not in correct state.";
    }
  },
};

module.exports = {
  Create,
  List
};
