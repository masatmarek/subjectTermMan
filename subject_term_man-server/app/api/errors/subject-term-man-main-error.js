"use strict";

const TermManUseCaseError = require("./term-man-use-case-error.js");
const SUBJECT_TERM_MAN_MAIN_ERROR_PREFIX = `${TermManUseCaseError.ERROR_PREFIX}subjectTermManMain/`;

const Load = {
  UC_CODE: `${SUBJECT_TERM_MAN_MAIN_ERROR_PREFIX}load/`,
  
};


module.exports = {
  Load
};
